---
title: CSS Layout Challenge
length:
tags: css, layout, positioning
---


### CSS Layout Challenge

As a front end developer, you will often be handed comps from a designer that you then have to build. You will need to be able to take a graphic file and recreate it with code. This means that you'll have to be able to think through how you're going to get all the elements in that comp where they need to be, and that requires a solid understanding of how to position HTML elements with CSS.

#### Getting Started

In this exercise, we'll take a series of simple comps and recreate each in [CodePen](http://codepen.io/). You will make the appropriate number of `<div>` tags with the div numbers indicated in the comps, give them a `<height>`, `<width>`, and `background-color` and then make them match the layout in the comp. Here's an example of what this might look like in your HTML and CSS:

Your HTML:

```HTML
<div class="div1">
  div1
</div>

<div class="div2">
  div2
</div>
```
Your CSS:

```CSS
div {
  width: 100px;
  height: 100px;
  background-color: black;
  color: white;
}
.div1 {
  float: right;
}

.div2 {
  float: left;
}
```

#### Tips:
* Remember, we've talked about several ways to position elements on the page. These include but are not necessarily limited to `display: block` and `display: inline-block`, `position: relative` and `position: absolute`, `margin`, `padding`, and the infamous `float`.
* If the layout dictates, some of the divs may be nested within other divs. Use your best judgement!
* See how many different ways you can achieve the same result and consider the pros/cons of solution!


### The Comps


##### Challenge 1:

![challenge 1](images/css1.png)


##### Challenge 2:

![challenge 2](images/css2.png)


##### Challenge 3:

![challenge 3](images/css3.png)


##### Challenge 4:

![challenge 4](images/css4.png)


##### Challenge 5:

![challenge 5](images/css5.png)


##### Challenge 6:

![challenge 6](images/css6.png)


##### Challenge 7:

![challenge 7](images/css7.png)


##### Challenge 8:

![challenge 8](images/css8.png)


##### Challenge 9:

![challenge 9](images/css9.png)


##### Challenge 10:

![challenge 10](images/css10.png)


##### Challenge 11:

![challenge 11](images/css11.png)


##### Challenge 12:

![challenge 12](images/css12.png)


##### Challenge 13:

![challenge 13](images/css13.png)


##### Challenge 14:

![challenge 14](images/css14.png)


##### Challenge 15:

![challenge 15](images/css15.png)

### Directory Structure:

                                CSS Drills
                                ├── CSS
                                │   ├── challenge_10.css
                                │   ├── challenge_11.css
                                │   ├── challenge_12.css
                                │   ├── challenge_13.css
                                │   ├── challenge_14.css
                                │   ├── challenge_15.css
                                │   ├── challenge_1.css
                                │   ├── challenge_2.css
                                │   ├── challenge_3.css
                                │   ├── challenge_4.css
                                │   ├── challenge_5.css
                                │   ├── challenge_6.css
                                │   ├── challenge_7.css
                                │   ├── challenge_8.css
                                │   └── challenge_9.css
                                ├── images
                                │   ├── css10.png
                                │   ├── css11.png
                                │   ├── css12.png
                                │   ├── css13.png
                                │   ├── css14.png
                                │   ├── css15.png
                                │   ├── css1.png
                                │   ├── css2.png
                                │   ├── css3.png
                                │   ├── css4.png
                                │   ├── css5.png
                                │   ├── css6.png
                                │   ├── css7.png
                                │   ├── css8.png
                                │   └── css9.png
                                ├── Pages
                                │   ├── challenge_10.html
                                │   ├── challenge_11.html
                                │   ├── challenge_12.html
                                │   ├── challenge_13.html
                                │   ├── challenge_14.html
                                │   ├── challenge_15.html
                                │   ├── challenge_1.html
                                │   ├── challenge_2.html
                                │   ├── challenge_3.html
                                │   ├── challenge_4.html
                                │   ├── challenge_5.html
                                │   ├── challenge_6.html
                                │   ├── challenge_7.html
                                │   ├── challenge_8.html
                                │   └── challenge_9.html
                                └── README.md

### Usage:

`Goto` `Pages` folder and `open` the `challenge page` and the `result` will be `displayed.`

Credits: https://github.com/turingschool-examples/css-layout-challenges
